<?php

namespace App\Filters;

use \Illuminate\Database\Eloquent\Builder;
use Kyslik\LaravelFilterable\GenericFilterable;

class Customer extends GenericFilterable
{

    /**
     * Filter mapping - map method to filter parameters
     *
     * @return array
     */
    public function filterMap(): array
    {
        return [
            'name' => ['name'],
            'country' => ['country'],
            'partner' => ['partner'],
            'description' => ['description'],
        ];
    }

    /**
     * Filter result by name
     *
     * @param string|null $name name
     *
     * @return Builder
     */
    public function name($name = null): Builder
    {
        return $this->builder->where('name', 'like', '%'.$name.'%');
    }

    /**
     * Filter result by country
     *
     * @param string|null $country country
     *
     * @return Builder
     */
    public function country($country = null): Builder
    {
        return $this->builder->select(['customers.*'])
            ->join('countries as c', 'customers.country_id', 'c.id')
            ->where('c.name', 'like', '%'.$country.'%');
    }

    /**
     * Filter result by partner
     *
     * @param string|null $partner partner
     *
     * @return Builder
     */
    public function partner($partner = null): Builder
    {
        return $this->builder->select(['customers.*'])
            ->join('partners as p', 'customers.partner_id', 'p.id')
            ->where('p.name', 'like', '%'.$partner.'%');
    }

    /**
     * Filter result by description
     *
     * @param string|null $description description
     *
     * @return Builder
     */
    public function description($description = null): Builder
    {
        return $this->builder->where('description', 'like', '%'.$description.'%');
    }
}