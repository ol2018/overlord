<?php

namespace App\Filters;

use App\Models\Services\QueryService;
use \Illuminate\Database\Eloquent\Builder;
use Kyslik\LaravelFilterable\GenericFilterable;

class DeviceHistory extends GenericFilterable
{

    /**
     * Filter mapping - map method to filter parameters
     *
     * @return array
     */
    public function filterMap(): array
    {
        return [
            'version' => ['version'],
            'lastIp' => ['last_ip'],
            'lastPtr' => ['last_ptr'],
            'lastCheck' => ['last_check'],
            'softwareComponent' => ['software_component'],
            'softwareBranch' => ['software_branch'],
        ];
    }

    /**
     * Filter result by version
     *
     * @param string|null $version version
     *
     * @return Builder
     */
    public function version($version = null): Builder
    {
        return $this->builder->select(['device_history.*'])
            ->join('device_history_software_version as dhsv', 'dhsv.device_history_id', 'device_history.id')
            ->join('software_versions as sv', 'sv.id', 'dhsv.software_version_id')
            ->where('sv.version', 'like', '%'.$version.'%')
            ->orderByDesc('device_history.id')
            ->groupBy(
                [
                    'device_history.id',
                    'device_history.device_id',
                    'device_history.ip',
                    'device_history.ptr',
                    'device_history.created_at',
                ]
            );
    }

    /**
     * Filter result by last ip
     *
     * @param string|null $lastIp last ip
     *
     * @return Builder
     */
    public function lastIp($lastIp = null): Builder
    {
        return $this->builder->where('ip', 'like', '%'.$lastIp.'%');
    }

    /**
     * Filter result by last ptr
     *
     * @param string|null $lastPtr last ptr
     *
     * @return Builder
     */
    public function lastPtr($lastPtr = null): Builder
    {
        return $this->builder->where('ptr', 'like', '%'.$lastPtr.'%');
    }

    /**
     * Filter result by last check
     *
     * @param string|null $lastCheck last check in format Y-m-d H:i:s
     *
     * @return Builder
     */
    public function lastCheck($lastCheck = null): Builder
    {
        return QueryService::dateTime($this->builder, 'created_at', $lastCheck);
    }

    /**
     * Filter result by software component
     *
     * @param string|null $softwareComponent software component
     *
     * @return Builder
     */
    public function softwareComponent($softwareComponent = null): Builder
    {
        return $this->builder->select(['device_history.*'])
            ->join('device_history_software_version as dhsv', 'dhsv.device_history_id', 'device_history.id')
            ->join('software_versions as sv', 'sv.id', 'dhsv.software_version_id')
            ->join('software_components as sc', 'sc.id', 'sv.software_component_id')
            ->where('sc.name', 'like', '%'.$softwareComponent.'%')
            ->orderByDesc('device_history.id')
            ->groupBy(
                [
                    'device_history.id',
                    'device_history.device_id',
                    'device_history.ip',
                    'device_history.ptr',
                    'device_history.created_at',
                ]
            );
    }


    /**
     * Filter result by software branch
     *
     * @param string|null $softwareBranch software branch
     *
     * @return Builder
     */
    public function softwareBranch($softwareBranch = null): Builder
    {
        return $this->builder->select(['device_history.*'])
            ->join('device_history_software_version as dhsv', 'dhsv.device_history_id', 'device_history.id')
            ->join('software_versions as sv', 'sv.id', 'dhsv.software_version_id')
            ->join('software_branches as sb', 'sb.id', 'sv.software_branch_id')
            ->where('sb.type', 'like', '%'.$softwareBranch.'%')
            ->orderByDesc('device_history.id')
            ->groupBy(
                [
                    'device_history.id',
                    'device_history.device_id',
                    'device_history.ip',
                    'device_history.ptr',
                    'device_history.created_at',
                ]
            );
    }
}