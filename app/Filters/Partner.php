<?php

namespace App\Filters;

use \Illuminate\Database\Eloquent\Builder;
use Kyslik\LaravelFilterable\GenericFilterable;

class Partner extends GenericFilterable
{

    /**
     * Filter mapping - map method to filter parameters
     *
     * @return array
     */
    public function filterMap(): array
    {
        return [
            'name' => ['name'],
            'country' => ['country'],
            'type' => ['type'],
        ];
    }

    /**
     * Filter result by name
     *
     * @param string|null $name name
     *
     * @return Builder
     */
    public function name($name = null): Builder
    {
        return $this->builder->where('name', 'like', '%'.$name.'%');
    }

    /**
     * Filter result by country
     *
     * @param string|null $country country
     *
     * @return Builder
     */
    public function country($country = null): Builder
    {
        return $this->builder->select(['partners.*'])
            ->join('countries as c', 'partners.country_id', 'c.id')
            ->where('c.name', 'like', '%'.$country.'%');
    }

    /**
     * Filter result by type
     *
     * @param string|null $type type
     *
     * @return Builder
     */
    public function type($type = null): Builder
    {
        return $this->builder->where('type', 'like', '%'.$type.'%');
    }
}