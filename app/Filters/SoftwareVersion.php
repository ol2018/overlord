<?php

namespace App\Filters;

use \Illuminate\Database\Eloquent\Builder;
use Kyslik\LaravelFilterable\GenericFilterable;

class SoftwareVersion extends GenericFilterable
{

    /**
     * Filter mapping - map method to filter parameters
     *
     * @return array
     */
    public function filterMap(): array
    {
        return [
            'version' => ['version'],
            'fileName' => ['file_name'],
            'softwareComponent' => ['software_component'],
            'softwareBranch' => ['software_branch'],
            'platform' => ['platforms'],
        ];
    }

    /**
     * Filter result by version
     *
     * @param string|null $version version
     *
     * @return Builder
     */
    public function version($version = null): Builder
    {
        return $this->builder->where('version', 'like', '%'.$version.'%');
    }

    /**
     * Filter result by file name
     *
     * @param string|null $fileName file name
     *
     * @return Builder
     */
    public function fileName($fileName = null): Builder
    {
        return $this->builder->where('file_name', 'like', '%'.$fileName.'%');
    }

    /**
     * Filter result by software component
     *
     * @param string|null $softwareComponent software component
     *
     * @return Builder
     */
    public function softwareComponent($softwareComponent = null): Builder
    {
        return $this->builder->select(['software_versions.*'])
            ->join('software_components as sc', 'software_versions.software_component_id', 'sc.id')
            ->where('sc.name', 'like', '%'.$softwareComponent.'%');
    }

    /**
     * Filter result by software branch
     *
     * @param string|null $softwareBranch software branch type
     *
     * @return Builder
     */
    public function softwareBranch($softwareBranch = null): Builder
    {
        return $this->builder->select(['software_versions.*'])
            ->join('software_branches as sb', 'software_versions.software_branch_id', 'sb.id')
            ->where('sb.type', 'like', '%'.$softwareBranch.'%');
    }

    /**
     * Filter result by platform name
     *
     * @param string|null $platform platform name
     *
     * @return Builder
     */
    public function platform($platform = null): Builder
    {
        return $this->builder->select(['software_versions.*'])
            ->join('platform_software_version as psv', 'software_versions.id', 'psv.software_version_id')
            ->join('platforms as pf', 'psv.platform_id', 'pf.id')
            ->where('pf.name', 'like', '%'.$platform.'%')
            ->groupBy(
                [
                    'software_versions.id',
                    'software_versions.software_component_id',
                    'software_versions.software_branch_id',
                    'software_versions.version',
                    'software_versions.file_name',
                    'software_versions.checksum',
                ]
            );
    }
}