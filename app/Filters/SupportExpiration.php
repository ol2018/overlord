<?php

namespace App\Filters;

use Illuminate\Database\Eloquent\Builder;
use Kyslik\LaravelFilterable\GenericFilterable;

class SupportExpiration extends GenericFilterable
{

    /**
     * Filter mapping - map method to filter parameters
     *
     * @return array
     */
    public function filterMap(): array
    {
        return [
            'description' => ['description'],
            'device' => ['device'],
            'supportType' => ['support_type'],
            'expirationDate' => ['expiration_date'],
        ];
    }

    /**
     * Filter result by description
     *
     * @param string|null $description description
     *
     * @return Builder
     */
    public function description($description = null): Builder
    {
        return $this->builder->where('description', 'like', '%'.$description.'%');
    }

    /**
     * Filter result by expiration date
     *
     * @param string|null $expirationDate expiration date
     *
     * @return Builder
     */
    public function expirationDate($expirationDate = null): Builder
    {
        $dayMin = '-01';
        $dayMax = '-31';
        $dayMonthMin = '-01-01';
        $dayMonthMax = '-12-31';
        $result = $this->builder;
        if (preg_match("/^\d{4}\-(0?[1-9]|1[012])$/", $expirationDate) === 1) {
            $result->where('expiration_date', '>=', $expirationDate.$dayMin)
                ->where('expiration_date', '<=', $expirationDate.$dayMax);
        } elseif (preg_match("/^\d{4}$/", $expirationDate) === 1) {
            $result->where('expiration_date', '>=', $expirationDate . $dayMonthMin)
                ->where('expiration_date', '<=', $expirationDate . $dayMonthMax);
        } else {
            $result->where('expiration_date', $expirationDate);
        }
        return $result;
    }

    /**
     * Filter result by support type
     *
     * @param string|null $supportType support type
     *
     * @return Builder
     */
    public function supportType($supportType = null): Builder
    {
        return $this->builder->select(['support_expiration.*'])
            ->join('support_types as st', 'support_type_id', 'st.id')
            ->where('st.name', 'like', '%'.$supportType.'%');
    }

    /**
     * Filter result by device serial
     *
     * @param string|null $device device serial
     *
     * @return Builder
     */
    public function device($device = null): Builder
    {
        return $this->builder->select(['support_expiration.*'])
            ->join('devices as ds', 'device_id', 'ds.id')
            ->where('ds.serial', 'like', '%'.$device.'%');
    }
}