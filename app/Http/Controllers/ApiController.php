<?php

namespace App\Http\Controllers;

use Response;

class ApiController extends Controller
{

    /**
     * Returns json response
     *
     * @param null $data     input data
     * @param int  $httpCode http code
     *
     * @return mixed
     */
    protected function respond($data = null, $httpCode = 200)
    {
        return Response::json(
            ['status' => $data, 'messages' => $this->model->getErrors()],
            $httpCode
        );
    }
}
