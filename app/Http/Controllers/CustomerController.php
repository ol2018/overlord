<?php

namespace App\Http\Controllers;

use Validator;
use App\Models\Customer;
use App\Filters\Customer as CustomerFilter;
use App\Http\Controllers\Traits\Show;
use App\Http\Controllers\Traits\Edit;
use App\Http\Controllers\Traits\Store;
use App\Http\Controllers\Traits\Create;
use App\Http\Controllers\Traits\Update;
use App\Http\Controllers\Traits\Destroy;

class CustomerController extends AdminController
{
    use Show,
        Edit,
        Store,
        Create,
        Update,
        Destroy;

    protected $model;

    /**
     * CustomerController constructor.
     *
     * @param Customer $model Customer model
     */
    public function __construct(Customer $model)
    {
        parent::__construct();
        $this->model = $model;
    }

    /**
     * Display a listing of the resource.
     *
     * @param CustomerFilter $filters filters
     *
     * @return \Illuminate\Http\Response
     */
    public function index(CustomerFilter $filters)
    {
        return view($this->model->getTable() . '.index', ['data' => $this->model->collectionItems($filters)]);
    }
}
