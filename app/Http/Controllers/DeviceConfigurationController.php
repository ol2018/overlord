<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Traits\Store;
use App\Http\Controllers\Traits\Create;
use App\Http\Controllers\Traits\Update;
use App\Http\Controllers\Traits\Destroy;
use App\Models\DeviceConfiguration;
use App\Filters\DeviceConfiguration as DeviceConfigurationFilter;

class DeviceConfigurationController extends AdminController
{
    use Store,
        Create,
        Update,
        Destroy;

    protected $model;

    /**
     * DeviceConfigurationController constructor.
     *
     * @param DeviceConfiguration $model Customer model
     */
    public function __construct(DeviceConfiguration $model)
    {
        parent::__construct();
        $this->model = $model;
    }

    /**
     * Display a listing of the resource.
     *
     * @param DeviceConfigurationFilter $filters filters
     *
     * @return \Illuminate\Http\Response
     */
    public function index(DeviceConfigurationFilter $filters)
    {
        return view($this->model->getTable() . '.index', ['data' => $this->model->collectionItems($filters)]);
    }

    /**
     * Display edit page
     *
     * @param Request $request request
     * @param string  $id      id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        return view($this->model->getTable() . '.edit', ['data' => $this->model->editItem($request, $id)]);
    }
}
