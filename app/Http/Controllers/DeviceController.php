<?php

namespace App\Http\Controllers;

use App\Models\Device;
use Illuminate\Http\Request;
use App\Filters\Device as DeviceFilter;
use App\Http\Controllers\Traits\Edit;
use App\Http\Controllers\Traits\Update;

class DeviceController extends AdminController
{
    use Edit,
        Update;

    protected $model;

    /**
     * DeviceController constructor.
     *
     * @param Device $model Device model
     */
    public function __construct(Device $model)
    {
        $this->middleware('auth')->except(['register', 'updateDevice', 'destroy']);
        $this->model = $model;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request Request object
     *
     * @return \Illuminate\Http\Response
     *
     * @throws \Throwable
     */
    public function register(Request $request)
    {
        return $this->model->register($request);
    }

    /**
     * Update resource in storage.
     *
     * @param Request $request Request object
     *
     * @return mixed
     *
     * @throws \Throwable
     */
    public function updateDevice(Request $request)
    {
        return $this->model->updateDevice($request);
    }

    /**
     * Display a listing of the resource.
     *
     * @param DeviceFilter $filters filters
     *
     * @return \Illuminate\Http\Response
     */
    public function index(DeviceFilter $filters)
    {
        return view($this->model->getTable() . '.index', ['data' => $this->model->collectionItems($filters)]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request request
     *
     * @return mixed
     */
    public function destroy(Request $request)
    {
        return $this->model->destroyEntity($request);
    }
}
