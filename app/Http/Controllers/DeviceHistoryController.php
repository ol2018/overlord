<?php

namespace App\Http\Controllers;

use App\Models\DeviceHistory;
use App\Filters\DeviceHistory as DeviceHistoryFilter;

class DeviceHistoryController extends AdminController
{
    protected $model;

    /**
     * DeviceController constructor.
     *
     * @param DeviceHistory $model DeviceHistory model
     */
    public function __construct(DeviceHistory $model)
    {
        parent::__construct();
        $this->model = $model;
    }

    /**
     * Display a listing of the resource.
     *
     * @param integer             $deviceId device id
     * @param DeviceHistoryFilter $filters  filters
     *
     * @return \Illuminate\Http\Response
     */
    public function history($deviceId, DeviceHistoryFilter $filters)
    {
        return view($this->model->getTable() . '.history', ['data' => $this->model->collectionItems($deviceId, $filters)]);
    }
}
