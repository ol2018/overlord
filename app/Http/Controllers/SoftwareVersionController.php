<?php

namespace App\Http\Controllers;

use Validator;
use App\Models\SoftwareVersion;
use App\Http\Controllers\Traits\Show;
use App\Http\Controllers\Traits\Edit;
use App\Http\Controllers\Traits\Store;
use App\Http\Controllers\Traits\Create;
use App\Http\Controllers\Traits\Update;
use App\Http\Controllers\Traits\Destroy;
use App\Filters\SoftwareVersion as SoftwareVersionFilter;

class SoftwareVersionController extends AdminController
{
    use Show,
        Edit,
        Store,
        Create,
        Update,
        Destroy;

    protected $model;

    /**
     * SoftwareVersionController constructor.
     *
     * @param SoftwareVersion $model SoftwareVersion model
     */
    public function __construct(SoftwareVersion $model)
    {
        parent::__construct();
        $this->model = $model;
    }

    /**
     * Display a listing of the resource.
     *
     * @param SoftwareVersionFilter $filters filters
     *
     * @return \Illuminate\Http\Response
     */
    public function index(SoftwareVersionFilter $filters)
    {
        return view($this->model->getTable() . '.index', ['data' => $this->model->collectionItems($filters)]);
    }

    /**
     * Return response download image file
     *
     * @param string $name image name
     *
     * @return array
     */
    public function downloadImage($name)
    {
        return $this->model->downloadImage($name);
    }
}
