<?php

namespace App\Http\Controllers;

use Validator;
use App\Models\SupportExpiration;
use App\Filters\SupportExpiration as SupportExpirationFilter;
use App\Http\Controllers\Traits\Edit;
use App\Http\Controllers\Traits\Store;
use App\Http\Controllers\Traits\Update;
use App\Http\Controllers\Traits\Destroy;

class SupportExpirationController extends AdminController
{
    use Edit,
        Store,
        Update,
        Destroy;

    protected $model;

    /**
     * SupportExpirationController constructor.
     *
     * @param SupportExpiration $model SupportExpiration model
     */
    public function __construct(SupportExpiration $model)
    {
        parent::__construct();
        $this->model = $model;
    }

    /**
     * Display a listing of the resource.
     *
     * @param SupportExpirationFilter $filters filters
     *
     * @return \Illuminate\Http\Response
     */
    public function index(SupportExpirationFilter $filters)
    {
        return view($this->model->getTable() . '.index', ['data' => $this->model->collectionItems($filters)]);
    }
}
