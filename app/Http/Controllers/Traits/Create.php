<?php

namespace App\Http\Controllers\Traits;

trait Create
{

    /**
     * Display create page
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view($this->model->getTable() . '.create', ['data' => $this->model->createItem()]);
    }
}
