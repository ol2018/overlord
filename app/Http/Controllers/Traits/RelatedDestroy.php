<?php

namespace App\Http\Controllers\Traits;

use App\Exceptions\NotFoundException;
use Illuminate\Routing\Route;

trait RelatedDestroy
{
    /**
     * Remove the specified resource from storage.
     *
     * @param string $id        entity id
     * @param string $relatedId related entity id
     * @param Route  $route     current rout
     *
     * @return \Illuminate\Http\Response
     *
     * @throws \Exception
     */
    public function destroy($id, $relatedId, Route $route)
    {
        return $this->respond($this->model->destroyRelation($id, $relatedId, $route));
    }

}
