<?php

namespace App\Http\Controllers\Traits;

use Illuminate\Routing\Route;

trait RelatedIndex
{
    /**
     * Display a listing of the resource relations.
     *
     * @param string $id    entity id
     * @param Route  $route current route
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id, Route $route)
    {
        return $this->respond($this->model->listRelations($id, $route));
    }
}
