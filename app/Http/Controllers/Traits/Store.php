<?php

namespace App\Http\Controllers\Traits;

use Validator;
use Illuminate\Http\Request;

trait Store
{
    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request Request object
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return $this->model->saveEntity($request);
    }
}
