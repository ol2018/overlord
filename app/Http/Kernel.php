<?php

namespace App\Http;

use App\Http\Middleware\AuthenticateOnceWithBasicAuth;
use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{
    /**
     * The application's global HTTP middleware stack.
     *
     * These middleware are run during every request to your application.
     *
     * @var array
     */
    protected $middleware = [
        \Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode::class,
        \Illuminate\Foundation\Http\Middleware\ValidatePostSize::class,
        \App\Http\Middleware\TrimStrings::class,
        \Illuminate\Foundation\Http\Middleware\ConvertEmptyStringsToNull::class,
        \App\Http\Middleware\TrustProxies::class,
    ];

    /**
     * The application's route middleware groups.
     *
     * @var array
     */
    protected $middlewareGroups = [
        'web' => [
            'start_session',
            'session_errors',
            'bindings',
        ],

        'api' => [
            'throttle:120,1',
            'bindings',
            'cors',
        ],
    ];

    /**
     * The application's route middleware.
     *
     * These middleware may be assigned to groups or used individually.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'start_session' => \Illuminate\Session\Middleware\StartSession::class,
        'authenticate_session' => \Illuminate\Session\Middleware\AuthenticateSession::class,
        'session_errors' => \Illuminate\View\Middleware\ShareErrorsFromSession::class,
        'bindings' => \Illuminate\Routing\Middleware\SubstituteBindings::class,
        'throttle' => \Illuminate\Routing\Middleware\ThrottleRequests::class,
        'guest' => \App\Http\Middleware\RedirectIfAuthenticated::class,
        'auth' => \Illuminate\Auth\Middleware\Authenticate::class,
        'apiAuth' => AuthenticateOnceWithBasicAuth::class,
        'cors' => \Barryvdh\Cors\HandleCors::class,
    ];
}
