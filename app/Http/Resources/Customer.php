<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class Customer extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request Request
     *
     * @return array
     */
    public function toArray($request)
    {
        unset($request);
        return [
            'country_id' => $this->getAttribute('country_id'),
            'countries' => self::getCountries(),
            'partner_id' => $this->getAttribute('partner_id'),
            'partners' => self::getPartners(),
            'id' => $this->getAttribute('id'),
            'resource' => $this->resource,
            'name' => $this->getAttribute('name'),
            'description' => $this->getAttribute('description'),
        ];
    }

    /**
     * Return assoc array of countries sorted by id
     *
     * @return mixed
     */
    protected static function getCountries()
    {
        $country = \App\Models\Country::pluck('name', 'id')->toArray();
        ksort($country);
        return $country;
    }

    /**
     * Return assoc array of partners sorted by id
     *
     * @return mixed
     */
    protected static function getPartners()
    {
        $country = \App\Models\Partner::pluck('name', 'id')->toArray();
        ksort($country);
        return $country;
    }
}
