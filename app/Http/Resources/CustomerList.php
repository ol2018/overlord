<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class CustomerList extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request Request
     *
     * @return array
     */
    public function toArray($request)
    {
        unset($request);
        return [
            'country' => $this->getAttribute('country')->getAttribute('name'),
            'partner' => $this->getAttribute('partner')->getAttribute('name'),
            'name' => $this->getAttribute('name'),
            'description' => $this->getAttribute('description'),
            'edit_button' =>  route($this->getTable() . '.edit', $this->getAttribute('id')),
            'delete_button' =>  [$this->getTable() . '.destroy', $this->getAttribute('id')],
        ];
    }
}
