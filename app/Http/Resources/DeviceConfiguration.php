<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class DeviceConfiguration extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request Request
     *
     * @return array
     */
    public function toArray($request)
    {
        $passwordInput = (!is_null($this->getAttribute('id')) and $request->has('display_password')) ? 'text' : 'password';
        return [
            'id' => $this->getAttribute('id'),
            'resource' => $this->resource,
            'device_id' => $this->getAttribute('device_id'),
            'devices' => self::getDevices(),
            'mgmt_ip' => $this->getAttribute('mgmt_ip'),
            'mgmt_login' => $this->getAttribute('mgmt_login'),
            'ip' => $this->getAttribute('ip'),
            'system_login' => $this->getAttribute('system_login'),
            'has_mgmt' => $this->getAttribute('has_mgmt'),
            'has_access' => $this->getAttribute('has_access'),
            'password_input' => $passwordInput,
        ];
    }

    /**
     * Return assoc array of devices sorted by id
     *
     * @return mixed
     */
    protected static function getDevices()
    {
        $devices = \App\Models\Device::pluck('serial', 'id')->toArray();
        ksort($devices);
        return $devices;
    }
}
