<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class DeviceConfigurationList extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request Request
     *
     * @return array
     */
    public function toArray($request)
    {
        unset($request);
        return [
            'device' => $this->getAttribute('device')->getAttribute('serial'),
            'mgmt_ip' => $this->getAttribute('mgmt_ip'),
            'mgmt_login' => $this->getAttribute('mgmt_login'),
            'ip' => $this->getAttribute('ip'),
            'system_login' => $this->getAttribute('system_login'),
            'has_mgmt' => ($this->getAttribute('has_mgmt') ? _i('True') : _i('False')),
            'has_access' => ($this->getAttribute('has_access') ? _i('True') : _i('False')),
            'edit_button' =>  route($this->getTable() . '.edit', $this->getAttribute('id')),
            'delete_button' =>  [$this->getTable() . '.destroy', $this->getAttribute('id')],
        ];
    }
}
