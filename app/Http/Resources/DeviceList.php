<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class DeviceList extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request Request
     *
     * @return array
     */
    public function toArray($request)
    {
        unset($request);
        $deviceHistory = $softwareVersion = null;
        if (!is_null($this->getAttribute('deviceHistory'))) {
            $deviceHistory =  $this->getAttribute('deviceHistory');
        }
        if (!is_null($deviceHistory)) {
            $softwareVersion = $deviceHistory->getAttribute('softwareVersions')->where('software_component_id', 1)->last();
        }
        return [
            'serial' => $this->getAttribute('serial'),
            'platform' => $this->getAttribute('platform')->getAttribute('name'),
            'customer' => $this->getAttribute('customer')->getAttribute('name'),
            'partner' => $this->getAttribute('customer')->getAttribute('partner')->getAttribute('name'),
            'version' => (!is_null($softwareVersion) ? $softwareVersion->getAttribute('version')  : ''),
            'device_status' => $this->getAttribute('deviceStatus')->getAttribute('name'),
            'last_ip' => (!is_null($deviceHistory) ? $deviceHistory->getAttribute('ip') : ''),
            'last_ptr' => (!is_null($deviceHistory) ? $deviceHistory->getAttribute('ptr') : ''),
            'last_check' => (!is_null($deviceHistory) ? $deviceHistory->getAttribute('created_at') : ''),
            'created_at' => $this->getAttribute('created_at'),
            'software_expiration' => ($this->getAttribute('softwareExpiration')->count() > 0 ? $this->getAttribute('softwareExpiration')->last()->getAttribute('expiration_date') : ''),
            'hardware_expiration' => ($this->getAttribute('hardwareExpiration')->count() > 0 ? $this->getAttribute('hardwareExpiration')->last()->getAttribute('expiration_date') : ''),
            'edit_button' =>  route($this->getTable() . '.edit', $this->getAttribute('id')),
            'device_history_button' =>  route('device_history.history', $this->getAttribute('id')),
        ];
    }
}
