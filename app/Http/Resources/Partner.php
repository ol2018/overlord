<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class Partner extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request Request
     *
     * @return array
     */
    public function toArray($request)
    {
        unset($request);
        return [
            'country_id' => $this->getAttribute('country_id'),
            'countries' => self::getCountries(),
            'id' => $this->getAttribute('id'),
            'resource' => $this->resource,
            'name' => $this->getAttribute('name'),
            'type' => $this->getAttribute('type'),
        ];
    }

    /**
     * Return assoc array of countries sorted by id
     *
     * @return mixed
     */
    protected static function getCountries()
    {
        $country = \App\Models\Country::pluck('name', 'id')->toArray();
        ksort($country);
        return $country;
    }
}
