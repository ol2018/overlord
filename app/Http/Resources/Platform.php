<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class Platform extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request Request
     *
     * @return array
     */
    public function toArray($request)
    {
        return parent::toArray($request);
    }
}
