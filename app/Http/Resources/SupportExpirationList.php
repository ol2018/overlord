<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class SupportExpirationList extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request Request
     *
     * @return array
     */
    public function toArray($request)
    {
        unset($request);
        return [
            'id' => $this->getAttribute('id'),
            'description' => $this->getAttribute('description'),
            'expiration_date' => $this->getAttribute('expiration_date'),
            'support_type' => $this->getAttribute('supportType')->getAttribute('name'),
            'device' => $this->getAttribute('device')->getAttribute('serial'),
            'edit_button' =>  route($this->getTable() . '.edit', $this->getAttribute('id')),
            'delete_button' =>  [$this->getTable() . '.destroy', $this->getAttribute('id')],
        ];
    }
}
