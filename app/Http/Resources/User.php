<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class User extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request Request
     *
     * @return array
     */
    public function toArray($request)
    {
        unset($request);
        return [
            'id' => $this->getAttribute('id'),
            'resource' => $this->resource,
            'name' => $this->getAttribute('name'),
            'email' => $this->getAttribute('email'),
            'ldap' => ($this->getAttribute('ldap') ? _i('True') : _i('False')),
        ];
    }
}
