<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class UserList extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request Request
     *
     * @return array
     */
    public function toArray($request)
    {
        unset($request);
        return [
            'name' => $this->getAttribute('name'),
            'email' => $this->getAttribute('email'),
            'ldap' => ($this->getAttribute('ldap') ? _i('True') : _i('False')),
            'edit_button' =>  route($this->getTable() . '.edit', $this->getAttribute('id')),
            'delete_button' =>  [$this->getTable() . '.destroy', $this->getAttribute('id')],
        ];
    }
}
