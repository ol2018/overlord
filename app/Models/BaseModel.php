<?php

namespace App\Models;

use Validator;
use Ramsey\Uuid\Uuid;
use Illuminate\Http\Request;
use OwenIt\Auditing\Auditable;
use Illuminate\Support\Facades\Auth;
use App\Exceptions\NotValidException;
use Watson\Validating\ValidatingTrait;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use OwenIt\Auditing\Contracts\UserResolver;

class BaseModel extends Model implements AuditableContract, UserResolver
{
    use Auditable,
        ValidatingTrait;

    /**
     * Standalone validation errors
     *
     * @var array
     */
    protected $errors;

    /**
     * Available model references
     *
     * @var array
     */
    protected $references = [];

    /**
     * Disable timestamps
     *
     * @var array
     */
    public $timestamps = false;

    /**
     * {@inheritdoc}
     *
     * @return mixed
     */
    public static function resolveId()
    {
        return Auth::check() ? Auth::user()->getAuthIdentifier() : null;
    }

    /**
     * Scope a query to include references.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query QueryBuilder object
     *
     * @return void
     */
    public function scopeWithReferences($query)
    {
        $query->with($this->references);
    }

    /**
     * Scope a query to include referenced entities.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query QueryBuilder object
     *
     * @return void
     */
    public function scopeWithReferencedBy($query)
    {
        $query->with($this->referencedBy);
    }

    /**
     * Returns model table name
     *
     * @return string
     */
    public static function getTableName()
    {
        return with(new static)->getTable();
    }

    /**
     * Get all of the models from the database.
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function listItems()
    {
        return self::withReferences()->get()->all();
    }

    /**
     * Rewrite already defined models rule if exist
     *
     * @param string $name  rule name
     * @param string $value rule value
     *
     * @return bool
     */
    public function rewriteRule($name, $value)
    {
        $result = false;
        $rules = $this->getRules();
        if (array_key_exists($name, $rules)) {
            $rules[$name] = $value;
            $this->setRules($rules);
            $result = true;
        }
        return $result;
    }

    /**
     * Validates uuid parameter
     *
     * @param string $id uuid id
     *
     * @return bool
     *
     * @throws NotValidException
     */
    public function validateUuid($id)
    {
        if (Uuid::isValid($id) === false) {
            throw new NotValidException('not_valid_uuid');
        }
        return true;
    }

    /**
     * Returns json inputs
     *
     * @param Request $request Request object
     * @param string  $id      Uuid string for validation
     *
     * @return array
     *
     * @throws NotValidException
     */
    public function getJsonInput(Request $request, $id = null)
    {
        if (!is_null($id)) {
            $this->validateUuid($id);
        }
        if ($request->getContentType() != 'json' or json_last_error() !== JSON_ERROR_NONE) {
            throw new NotValidException('not_valid_json');
        }
        return $request->json()->all();
    }

    /**
     * Standalone model validation
     *
     * @return bool
     */
    public function validate()
    {
        // make a new validator object
        $this->updateRulesUniques();
        $validator = Validator::make($this->getAttributes(), $this->getRules());
        if ($validator->fails()) {
            // set errors and return false
            $errors = [];
            if (isset($this->id)) {
                $errors['id'] = $this->id;
            }
            $messages = $validator->errors()->messages();
            $errors['attributes'] = array_only($this->getAttributes(), array_keys($messages));
            $errors['errors'] = $messages;
            $this->errors = $errors;
            return false;
        }
        return true;
    }

    /**
     * Standalone validation errors getter
     *
     * @return array
     */
    public function getErrors()
    {
        return $this->errors;
    }
}