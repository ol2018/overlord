<?php

namespace App\Models;

use App\Models\Traits\ShowEntity;
use App\Models\Traits\SaveEntity;
use App\Models\Traits\DestroyEntity;

class Contact extends BaseModel
{
    use ShowEntity,
        SaveEntity,
        DestroyEntity;

    /**
     * Table name
     *
     * @var string
     */
    protected $table = 'contacts';

    /**
     * Validating rules
     *
     * @var array
     */
    protected $rules = [
        'customer_id' => 'required|integer|exists:customers,id',
        'partner_id' => 'required|integer|exists:partners,id',
        'name'  =>  'required|unique:contacts|min:1|max:45',
        'email' => 'required|email|max:255|unique:contacts',
        'phone' => 'max:60',
        'description' => 'max:255',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'country_id',
        'partner_id',
        'name',
        'email',
        'phone',
        'description',
    ];
}
