<?php

namespace App\Models;

use Response;
use Validator;
use Illuminate\Http\Request;
use App\Models\Traits\SaveEntity;
use App\Models\Services\UpdateService;
use App\Models\Services\VersionService;
use App\Filters\Device as DeviceFilter;
use App\Models\Services\RegisterService;
use App\Models\Services\ValidationService;
use Kyslik\LaravelFilterable\FilterableTrait;
use App\Http\Resources\Device as DeviceResource;
use App\Http\Resources\DeviceList;

class Device extends BaseModel
{
    use SaveEntity,
        FilterableTrait;

    /**
     * Boot function from laravel.
     *
     * @return void
     */
    protected static function boot()
    {

        parent::boot();

        static::created(
            function ($model) {
                SupportExpiration::addSoftware($model);
                SupportExpiration::addHardware($model);
            }
        );
    }

    /**
     * Enable timestamps
     *
     * @var array
     */
    public $timestamps = true;

    /**
     * Table name
     *
     * @var string
     */
    protected $table = 'devices';

    /**
     * Validating rules
     *
     * @var array
     */
    protected $rules = [
        'customer_id' => 'required|integer|exists:customers,id',
        'software_branch_id' => 'required|integer|exists:software_branches,id',
        'device_status_id' => 'required|integer|exists:device_statuses,id',
        'platform_id' => 'required|integer|exists:platforms,id',
        //'uuid'  =>  'required|uuid|unique:devices',
        'serial'  =>  'required|unique:devices|min:1|max:45',
        'description' => 'max:255',
        //'hw_model' => 'max:255',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'customer_id',
        'software_branch_id',
        'device_status_id',
        'platform_id',
        //'uuid',
        'serial',
        'description',
        //'hw_model',
    ];

    /**
     * Hidden columns from output
     *
     * @var array
     */
    protected $hidden = [
        'updated_at',
        'created_at',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * Register new device
     *
     * @param Request $request input params
     *
     * @return \Illuminate\Http\Response
     *
     * @throws \Throwable
     */
    public function register(Request $request)
    {
        $result = ['status' => 'ok'];
        $code = 200;
        $inputParams = $this->getParams($request);
        $params = RegisterService::getData($inputParams);
        $model = $this;
        $model->fill($params);
        $validator = Validator(array_only($model->getAttributes(), 'serial'), array_only($model->getRules(), 'serial'));
        $registerValidator = RegisterService::getValidator();
        if ($registerValidator->fails()) {
            $validator = ValidationService::addValidationAfterHook($validator, $registerValidator);
        }
        if ($validator->fails()) {
            $result['status'] = 'error';
            $result['message'] = array_flatten($validator->errors()->getMessages());
            $code = 400;
        } else {
            $model->saveOrFail();
        }
        return Response::json($result, $code);
    }

    /**
     * Update device
     *
     * @param Request $request input params
     *
     * @return array
     *
     * @throws \Throwable
     */
    public function updateDevice(Request $request)
    {
        $inputParams = $this->getParams($request);
        $result = ['status' => 'ok'];
        $code = 200;
        $validator = UpdateService::getValidator($inputParams);
        if ($validator->fails()) {
            $result['status'] = 'error';
            $result['message'] = array_flatten($validator->errors()->getMessages());
            $code = 400;
        } else {
            $softwareVersion = UpdateService::getSoftwareVersion();
            $result['version'] = $softwareVersion->getAttribute('version');
            $result['url'] = $softwareVersion::getImageUrl($softwareVersion->getAttribute('file_name'));
        }
        // add device history row
        $device = Device::where('serial', array_get($inputParams, 'lmserial'))->get()->first();
        if (!is_null($device)) {
            $inputVersion = SoftwareVersion::where('version', array_get($inputParams, 'version'))->first();
            $historyVersionId = is_null($inputVersion) ? 1 : data_get($inputVersion, 'id');
            DeviceHistory::addItem($device->getAttribute('id'), $historyVersionId);
        }
        return Response::json($result, $code);
    }

    /**
     * Get all of the models from the database.
     *
     * @param DeviceFilter $filters filters
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function collectionItems(DeviceFilter $filters)
    {
        $items = self::with(
            [
                'platform',
                'customer',
                'deviceStatus',
                'softwareExpiration',
                'hardwareExpiration',
                'deviceHistory',
            ]
        )->filter($filters)->get();
        return DeviceList::collection($items);
    }

    /**
     * Return resource item data
     *
     * @param string $id id
     *
     * @return array
     */
    public function editItem($id)
    {
        $res = new DeviceResource($this->find($id));
        return $res->toArray(0);
    }

    /**
     * Destroy entity
     *
     * @param Request $request request
     *
     * @return Response
     */
    public function destroyEntity(Request $request)
    {
        $params = $this->getParams($request);
        $rules = [
            'serial' => 'required|exists:devices,serial',
            'platform' => 'required|exists:platforms,name',
        ];
        Validator::make(array_only($params, ['serial', 'platform']), $rules)->validate();
        $platform = Platform::where('name', array_get($params, 'platform'))->first();
        $device = Device::where('serial', array_get($params, 'serial'))->where('platform_id', data_get($platform, 'id'))->first();
        $device->delete();
        return Response::json(['status' => 'ok'], 200);
    }

    /**
     * Define BelongsTo relation
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function platform()
    {
        return $this->belongsTo(Platform::class);
    }

    /**
     * Define BelongsTo relation
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    /**
     * Define BelongsTo relation
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function deviceStatus()
    {
        return $this->belongsTo(DeviceStatus::class);
    }

    /**
     * Define HasMany relation
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function softwareExpiration()
    {
        return $this->hasMany(SupportExpiration::class)->where(['support_type_id' => 1]);
    }

    /**
     * Define HasMany relation
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function hardwareExpiration()
    {
        return $this->hasMany(SupportExpiration::class)->where(['support_type_id' => 2]);
    }

    /**
     * Define HasOne relation
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function deviceHistory()
    {
        return $this->hasOne(DeviceHistory::class)->orderByDesc('id');
    }

    /**
     * Return input params from request
     *
     * @param Request $request Request
     *
     * @return array
     */
    protected function getParams(Request $request): array
    {
        $result = (array) json_decode($request->getContent());
        if ($request->getContentType() == 'json') {
            $result = $request->all();
        }
        if (array_has($result, 'version')) {
            $result['version'] = VersionService::encode(array_get($result, 'version'));
        }
        if (array_has($result, 'lmversion')) {
            $result['lmversion'] = VersionService::encode(array_get($result, 'lmversion'));
        }
        return $result;
    }
}
