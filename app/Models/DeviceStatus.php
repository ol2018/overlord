<?php

namespace App\Models;

use App\Models\Traits\ShowEntity;
use App\Models\Traits\SaveEntity;
use App\Models\Traits\DestroyEntity;

class DeviceStatus extends BaseModel
{
    use ShowEntity,
        SaveEntity,
        DestroyEntity;

    /**
     * Table name
     *
     * @var string
     */
    protected $table = 'device_statuses';

    /**
     * Validating rules
     *
     * @var array
     */
    protected $rules = [
        'name'  =>  'required|unique:device_statuses|min:1|max:45',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
    ];
}
