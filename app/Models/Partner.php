<?php

namespace App\Models;

use App\Models\Traits\ShowEntity;
use App\Models\Traits\SaveEntity;
use App\Models\Traits\DestroyEntity;
use Kyslik\LaravelFilterable\FilterableTrait;
use App\Http\Resources\Partner as PartnerResource;
use App\Http\Resources\PartnerList;
use App\Filters\Partner as PartnerFilter;

class Partner extends BaseModel
{
    use ShowEntity,
        SaveEntity,
        DestroyEntity,
        FilterableTrait;

    /**
     * Table name
     *
     * @var string
     */
    protected $table = 'partners';

    /**
     * Validating rules
     *
     * @var array
     */
    protected $rules = [
        'country_id' => 'required|integer|exists:countries,id',
        'name'  =>  'required|unique:software_components|min:1|max:45',
        'type'  =>  'required|min:1|max:45',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'country_id',
        'name',
        'type',
    ];

    /**
     * Get all of the models from the database.
     *
     * @param PartnerFilter $filters filters
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function collectionItems(PartnerFilter $filters)
    {
        $items = self::filter($filters)->get();
        return PartnerList::collection($items);
    }

    /**
     * Return resource item data
     *
     * @param string $id id
     *
     * @return array
     */
    public function editItem($id)
    {
        $res = new PartnerResource($this->find($id));
        return $res->toArray(0);
    }

    /**
     * Return resource item data
     *
     * @return array
     */
    public function createItem()
    {
        $res = new PartnerResource(new self());
        return $res->toArray(0);
    }

    /**
     * Define BelongsTo relation
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function country()
    {
        return $this->belongsTo(Country::class);
    }
}
