<?php

namespace App\Models\Services;

use Validator;
use App\Models\Platform;

class RegisterService
{
    /**
     * Validator
     *
     * @var Validator
     */
    protected static $validator;

    /**
     * Validating rules
     *
     * @var array
     */
    protected static $rules = [
        'platform' => 'required|exists:platforms,name',
        'serial'  =>  'required|unique:devices',
        'version' => 'required|exists:software_versions',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected static $fillable = [
        'platform',
        'serial',
        'version',
    ];

    /**
     * Return transformed data
     *
     * @param array $params input params
     *
     * @return array valid data
     *
     * @throws \Throwable
     */
    public static function getData(array $params): array
    {
        $parameters = array_only($params, self::$fillable);
        self::$validator = Validator::make($parameters, self::$rules);
        return [
            'customer_id' => 1,
            'platform_id' => self::getPlatformIdBy(array_get($parameters, 'platform')),
            'software_branch_id' => 1,
            'device_status_id' => 1,
            'serial' => array_get($parameters, 'serial'),
        ];
    }

    /**
     * Return self validator
     *
     * @return Validator
     */
    public static function getValidator()
    {
        return self::$validator;
    }

    /**
     * Return platform id by name
     *
     * @param string $name platform name
     *
     * @return mixed
     */
    protected static function getPlatformIdBy($name)
    {
        $model = Platform::where('name', $name)->first();
        return !is_null($model) ? $model->id : null;
    }
}
