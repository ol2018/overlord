<?php

namespace App\Models\Services;

use DB;
use Validator;
use App\Models\Device;
use App\Models\SoftwareVersion;
use App\Models\SoftwareComponent;
use App\Models\SupportExpiration;
use App\Models\SoftwareVersionCompatibility;

class UpdateService
{
    protected static $softwareVersion;

    /**
     * Validating rules
     *
     * @var array
     */
    protected static $rules = [
        'lmserial' => 'required|exists:devices,serial',
        'support_expiration' => 'required|is_expired',
        'version' => 'required|latest_version',
        'lmversion' => 'required|exists:software_versions,version',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected static $fillable = [
        'lmserial',
        'support_expiration',
        'version',
        'lmversion',
    ];

    /**
     * Return first validator with error
     *
     * @param array $params input params
     *
     * @return mixed
     *
     * @throws \Throwable
     */
    public static function getValidator($params)
    {
        $validator = Validator::make(array_only($params, 'lmserial'), array_only(self::$rules, 'lmserial'));
        if ($validator->fails()) {
            return $validator;
        }
        $data = ['support_expiration' => self::getSupportExpiration(array_get($params, 'lmserial'))];
        $validator = Validator::make($data, array_only(self::$rules, 'support_expiration'));
        if ($validator->fails()) {
            return $validator;
        }

        $validator = Validator::make(
            array_only($params, 'lmversion'),
            array_only(self::$rules, 'lmversion')
        );
        if ($validator->fails()) {
            return $validator;
        }

        $rules = array_get(self::$rules, 'version') . ':' . self::getDatabaseVersion($params);
        $validator = Validator::make(
            array_only($params, 'version'),
            ['version' => $rules]
        );
        $validator->passes();
        return $validator;
    }

    /**
     * Return software support expiration date or null
     *
     * @param string $serial serial
     *
     * @return date|null
     */
    protected static function getSupportExpiration($serial)
    {
        $model = SupportExpiration::where('device_id', self::getDeviceIdBy($serial))->where('support_type_id', 1)->first();
        return !is_null($model) ? $model->getAttribute('expiration_date') : null;
    }


    /**
     * Return latest database software version by input params
     *
     * @param array $params input params
     *
     * @return string|null
     */
    protected static function getDatabaseVersion($params)
    {
        $device = self::getDeviceBy(array_get($params, 'lmserial'));
        $versions = DB::table('software_versions')
            ->select('id', 'version')
            ->where('software_branch_id', $device->getAttribute('software_branch_id'))
            ->where('software_component_id', self::getSoftwareComponentIdBy(array_get($params, 'product')))
            ->where('is_enabled', true)
            ->get();
        $version = self::getAvailableVersion($versions, array_get($params, 'lmversion'));
        self::$softwareVersion = $version;
        return !is_null($version) ?  $version->getAttribute('version') : null;
    }

    /**
     * Return available database software version by compatibility
     *
     * @param array  $componentVersions input params
     * @param string $lmVersion         version from request
     *
     * @return string|null
     */
    protected static function getAvailableVersion($componentVersions, $lmVersion)
    {
        $result = null;
        if ($componentVersions->count() == 0) {
            return $result;
        }
        $allVersions = SoftwareVersion::pluck('version', 'id')
            ->map(
                function ($item) {
                    return VersionService::encode($item);
                }
            );
        $sortedVersions = $componentVersions->sortByDesc('version');
        foreach ($sortedVersions as $version) {
            $compatibility = SoftwareVersionCompatibility::where('software_version_id', data_get($version, 'id'))->get();
            $compatible = true;
            if ($compatibility->count() > 0) {
                foreach ($compatibility as $item) {
                    $minVersion = $allVersions->get($item->getAttribute('min_version_id'));
                    $maxVersion = $allVersions->get($item->getAttribute('max_version_id'));
                    if (!is_null($minVersion) and $lmVersion < $minVersion) {
                        $compatible = false;
                        break;
                    }
                    if (!is_null($maxVersion) and $lmVersion > $maxVersion) {
                        $compatible = false;
                        break;
                    }
                }
            }
            if ($compatible) {
                $result = SoftwareVersion::find(data_get($version, 'id'));
                break;
            }
        }
        return $result;
    }

    /**
     * Return device or null
     *
     * @param string $serial serial
     *
     * @return Device|null
     */
    protected static function getDeviceBy($serial)
    {
        return Device::where('serial', $serial)->first();
    }

    /**
     * Return device id or null
     *
     * @param string $serial serial
     *
     * @return string|null
     */
    protected static function getDeviceIdBy($serial)
    {
        $model = self::getDeviceBy($serial);
        return !is_null($model) ? $model->getAttribute('id') : null;
    }

    /**
     * Return software component id or null
     *
     * @param string $product product
     *
     * @return string|null
     */
    protected static function getSoftwareComponentIdBy($product)
    {
        $model = SoftwareComponent::where('name', strtolower($product))->first();
        return !is_null($model) ? $model->getAttribute('id') : null;
    }

    /**
     * Return SoftwareVersion
     *
     * @return mixed
     */
    public static function getSoftwareVersion()
    {
        return self::$softwareVersion;
    }
}
