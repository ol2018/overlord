<?php

namespace App\Models\Services;

use App\Models\BaseModel;
use Illuminate\Validation\Validator;
use Illuminate\Database\Eloquent\Model;

class ValidationService extends Model
{
    protected static $model;

    /**
     * Prepare validator for output
     *
     * @param BaseModel $model base model object
     *
     * @return Validator
     */
    public static function prepareValidator(BaseModel $model): Validator
    {
        $model->updateRulesUniques();
        self::$model = $model;
        $validator = Validator($model->getAttributes(), $model->getRules());
        $relationsValidator = self::createRelationsValidator();
        if ($relationsValidator->fails()) {
            $validator = self::addValidationAfterHook($validator, $relationsValidator);
        }
        return $validator;
    }

    /**
     * Return relation validator
     *
     * @return Validator
     */
    protected static function createRelationsValidator(): Validator
    {
        $model = self::$model;
        $relationsAttributes = $relationsRules = [];
        if (!is_null($model->relations)) {
            foreach ($model->relations as $relation => $ids) {
                $relationsAttributes[$relation] = $ids;
                $relationsRules[$relation] = 'not_found_ids:' . $relation;
            }
        }
        return Validator($relationsAttributes, $relationsRules);
    }

    /**
     * Add validation after hook for merging messages
     *
     * @param Validator $validator          model validator
     * @param Validator $relationsValidator relations validator
     *
     * @return Validator
     */
    public static function addValidationAfterHook(Validator $validator, Validator $relationsValidator): Validator
    {
        return $validator->after(
            function ($validator) use ($relationsValidator) {
                $messages = $relationsValidator->errors()->messages();
                foreach ($messages as $relatedEntity => $message) {
                    $validator->errors()->add($relatedEntity, reset($message));
                }
            }
        );
    }
}