<?php

namespace App\Models;

use App\Models\Traits\ShowEntity;
use App\Models\Traits\SaveEntity;
use App\Models\Traits\DestroyEntity;

class SoftwareComponent extends BaseModel
{
    use ShowEntity,
        SaveEntity,
        DestroyEntity;

    /**
     * Table name
     *
     * @var string
     */
    protected $table = 'software_components';

    /**
     * Validating rules
     *
     * @var array
     */
    protected $rules = [
        'software_component_type_id' => 'required|integer|exists:software_component_types,id',
        'name'  =>  'required|unique:software_components|min:1|max:45',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'software_component_type_id',
        'name',
    ];
}
