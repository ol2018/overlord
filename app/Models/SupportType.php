<?php

namespace App\Models;

use App\Models\Traits\SaveEntity;
use App\Models\Traits\DestroyEntity;

class SupportType extends BaseModel
{
    use SaveEntity,
        DestroyEntity;

    /**
     * Table name
     *
     * @var string
     */
    protected $table = 'support_types';

    /**
     * Validating rules
     *
     * @var array
     */
    protected $rules = [
        'name'  =>  'required|unique:support_types|min:1|max:45',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
    ];
}
