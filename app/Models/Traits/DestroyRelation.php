<?php

namespace App\Models\Traits;

use Illuminate\Routing\Route;
use Illuminate\Support\Facades\DB;

trait DestroyRelation
{

    /**
     * Destroy entity relation
     *
     * @param string $id        entity id
     * @param string $relatedId related id
     * @param Route  $route     current route
     *
     * @return array
     */
    public function destroyRelation($id, $relatedId, $route)
    {
        $model = $this->findOrFail($id);
        $aRelatedEntity = explode(".", $route->getName());
        $relation = (isset($aRelatedEntity[1]) and strlen($aRelatedEntity[1]) > 0) ? camel_case($aRelatedEntity[1]) : null;

        DB::transaction(
            function () use ($model, $relation, $relatedId) {
                self::validateRelations($model, $relation, [['id' => $relatedId]]);
                $model->$relation()->detach($relatedId);
                if (method_exists($model, 'updateIsActive')) {
                    $model->updateIsActive();
                }
            }
        );
        return $model->$relation->toArray();
    }
}