<?php

namespace App\Models\Traits;

trait DestroyRelations
{

    /**
     * Destroy all entity relations
     *
     * @param string $id entity id
     *
     * @return bool
     */
    public function destroyRelations($id)
    {
        $result = false;
        $model = $this->findOrFail($id);
        $references = $model->getReferences();
        if (count($references) > 0) {
            foreach ($references as $relation) {
                if (method_exists($model, $relation)) {
                    if (method_exists($model->$relation(), 'detach')) {
                        $model->$relation()->detach();
                        $result = true;
                    }
                }
            }
        }
        return $result;
    }
}