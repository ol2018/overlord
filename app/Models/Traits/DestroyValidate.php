<?php

namespace App\Models\Traits;

use Validator;

trait DestroyValidate
{
    /**
     * Destroy entity
     *
     * @param object $model entity object
     *
     * @return mixed
     */
    public static function destroyValidate($model)
    {
        $attributes = $model->getAttributes();
        $rules = [];
        if (!is_null($model->isReferenced)) {
            $attributes['is_referenced'] = $model->isReferenced;
            $rules['is_referenced'] = 'is_referenced';
        }
        Validator($attributes, $rules)->validate();
        return true;
    }
}