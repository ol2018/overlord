<?php

namespace App\Models\Traits;

use Ramsey\Uuid\Uuid;
use Illuminate\Support\Facades\DB;

trait ValidateRelations
{
    /**
     * Validates relations
     *
     * @param object $model    entity object
     * @param string $relation entity relation
     * @param array  $params   related ids
     *
     * @return boolean
     */
    public static function validateRelations($model, $relation, $params)
    {
        $relationsAttributes = [$relation => array_pluck($params, 'id')];
        $relationsRules = [
            $relation => 'not_found_ids:' . $model->getTable(),
        ];
        $relationsValidator = Validator($relationsAttributes, $relationsRules);
        $validator = Validator($params, []);
        $validator->after(
            function ($validator) use ($relationsValidator, $model, $relationsAttributes) {
                if ($relationsValidator->fails()) {
                    $messages = $relationsValidator->errors()->messages();
                    foreach ($messages as $relatedEntity => $message) {
                        $notValidIds = $dbIds = [];
                        $ids = $relationsAttributes[$relatedEntity];
                        foreach ($ids as $index => $id) {
                            if (Uuid::isValid($id) === false) {
                                unset($ids[$index]);
                                $notValidIds[] = $id;
                            }
                        }
                        $collection = DB::table($model->$relatedEntity()->getModel()->getTable())->where('deleted_at')->whereIn('id', $ids)->get(['id']);
                        $dbIds = $collection->map(
                            function ($item) {
                                return $item->id;
                            }
                        )->all();
                        $notFoundIds = array_merge($notValidIds, array_diff($ids, $dbIds));
                        $resultMessage = [
                            "ids" => $notFoundIds,
                            "error" => $message
                        ];
                        $validator->errors()->add($relatedEntity, $resultMessage);
                    }
                }
            }
        );
        $validator->validate();
        return true;
    }
}