<?php

namespace App\Models;

use Hash;
use Illuminate\Http\Request;
use Adldap\Laravel\Facades\Adldap;
use Illuminate\Auth\Authenticatable;
use Adldap\Models\User as LdapUser;
use Illuminate\Notifications\Notifiable;
use Kyslik\LaravelFilterable\FilterableTrait;
use App\Http\Resources\User as UserResource;
use App\Http\Resources\UserList;
use App\Filters\User as UserFilter;
use App\Models\Traits\ShowEntity;
use App\Models\Traits\SaveEntity;
use App\Models\Traits\DestroyEntity;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;

class User extends BaseModel implements AuthenticatableContract
{
    use Notifiable,
        ShowEntity,
        SaveEntity,
        DestroyEntity,
        Authenticatable,
        FilterableTrait;

    /**
     * Table name
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * Validating rules
     *
     * @var array
     */
    protected $rules = [
        'name'  =>  'required|unique:users|min:1|max:35',
        'email' => 'email|max:255|unique:users',
        'password' => 'required|min:1|max:255',
        'ldap' => 'required|boolean',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'ldap',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * Hash password before save
     *
     * @param string $pass password
     *
     * @return void
     */
    public function setPasswordAttribute($pass)
    {
        $this->attributes['password'] = Hash::make($pass);
        if (is_null($pass)) {
            unset($this->attributes['password']);
        }
    }

    /**
     * Attempt login LDAP user
     *
     * @param Request $request request
     *
     * @return bool
     */
    public static function attemptLdapUser(Request $request)
    {
        $result = false;
        $params = $request->all();
        $name = array_get($params, 'name');
        $password = array_get($params, 'password');
        if (Adldap::auth()->attempt($name, $password)
            and ($ldapUser = self::getLdapUser($name, $password))
        ) {
            self::createUpdateLdapUser($ldapUser, $password);
            $result = true;
        }
        return $result;
    }

    /**
     * Create or update, if exists, ldap user in local database
     *
     * @param LdapUser $ldapUser ldap User
     * @param string   $password password
     *
     * @return void
     */
    protected static function createUpdateLdapUser(LdapUser $ldapUser, $password)
    {
        $dbUser = User::firstOrNew(
            ['name' => strtolower($ldapUser->getAccountName())],
            ['ldap' => true]
        );
        $dbUser->fill(
            [
                'email' => $ldapUser->getEmail(),
                'password' => $password
            ]
        );
        $dbUser->save();
    }

    /**
     * Return Ldap user if exists and is in group Sirwisa-overlord or return null
     *
     * @param string $name     name
     * @param string $password password
     *
     * @return mixed
     */
    protected static function getLdapUser($name, $password)
    {
        Adldap::auth()->bind($name, $password);
        $user = Adldap::search()->find($name);
        return $user->inGroup('Sirwisa-overlord') ? $user : null;
    }

    /**
     * Get all of the models from the database.
     *
     * @param UserFilter $filters filters
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function collectionItems(UserFilter $filters)
    {
        $items = self::filter($filters)->get();
        return UserList::collection($items);
    }

    /**
     * Return resource item data
     *
     * @param string $id id
     *
     * @return array
     */
    public function editItem($id)
    {
        $res = new UserResource($this->find($id));
        return $res->toArray(0);
    }

    /**
     * Return resource item data
     *
     * @return array
     */
    public function createItem()
    {
        $res = new UserResource(new self());
        return $res->toArray(0);
    }
}
