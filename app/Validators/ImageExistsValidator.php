<?php

namespace App\Validators;

use Config;

class ImageExistsValidator implements ValidatorInterface
{

    /**
     * Checks whether the image file exists in defined directory
     *
     * @param string $value      input value
     * @param array  $attribute  array of attributes
     * @param array  $parameters array of params
     * @param object $validator  instance of Illuminate\Support\Facades\Validator
     *
     * @return bool
     */
    public static function validate($value, $attribute, $parameters, $validator)
    {
        unset($attribute, $parameters, $validator);
        $imagesPath = Config::get('constants.VERSION_IMAGES_PATH');
        return file_exists($imagesPath . DIRECTORY_SEPARATOR . $value);
    }
}