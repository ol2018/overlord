<?php

namespace App\Validators;

use Validator;

class IsExpiredValidator implements ValidatorInterface
{

    /**
     * Checks whether the value under validation is expired
     *
     * @param string $value      input value
     * @param array  $attribute  array of attributes
     * @param array  $parameters array of params
     * @param object $validator  instance of Illuminate\Support\Facades\Validator
     *
     * @return bool
     */
    public static function validate($value, $attribute, $parameters, $validator)
    {
        unset($validator, $parameters);
        $params = [date('Y-m-d')];
        $validator = Validator::make([], []);
        return $validator->validateAfterOrEqual($attribute, $value, $params);
    }
}