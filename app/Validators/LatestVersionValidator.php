<?php

namespace App\Validators;

use Validator;
use App\Models\Services\VersionService;

class LatestVersionValidator implements ValidatorInterface
{

    /**
     * Checks whether the version is latest
     *
     * @param string $value      input value
     * @param array  $attribute  array of attributes
     * @param array  $parameters array of params
     * @param object $validator  instance of Illuminate\Support\Facades\Validator
     *
     * @return bool
     */
    public static function validate($value, $attribute, $parameters, $validator)
    {
        unset($attribute, $validator);
        // db version
        $version1 = VersionService::encode(array_get($parameters, '0'));
        // input request version
        $version2 = VersionService::encode($value);

        return ($version1 > $version2);
    }
}