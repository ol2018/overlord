<?php

namespace App\Validators;


interface ValidatorInterface
{
    /**
     * Validators interface
     *
     * @param string $value      input value
     * @param array  $attribute  array of attributes
     * @param array  $parameters array of params
     * @param object $validator  instance of Illuminate\Support\Facades\Validator
     *
     * @return bool
     */
    public static function validate($value, $attribute, $parameters, $validator);
}