<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Connections
    |--------------------------------------------------------------------------
    |
    | This array stores the connections that are added to Adldap. You can add
    | as many connections as you like.
    |
    | The key is the name of the connection you wish to use and the value is
    | an array of configuration settings.
    | $ ldapsearch -x -h x.compunet.cz -D a -w a -b "cn=a,cn=Users,dc=x,dc=compunet,dc=cz" name
    | $ ldapsearch -x -LLL -H ldap://server.example.com -b dc=example,dc=com
    | $ ldapsearch -x -LLL -H ldap://ldap.example.com -D cn=admin,dc=example,dc=com -w admin -b dc=example,dc=com
    |
    */

    'connections' => [

        'default' => [
            'auto_connect' => env('ADLDAP_AUTO_CONNECT', true),
            'connection' => Adldap\Connections\Ldap::class,
            'schema' => Adldap\Schemas\ActiveDirectory::class,
            'connection_settings' => [
                'account_prefix' => env('ADLDAP_ACCOUNT_PREFIX', ''),
                'account_suffix' => env('ADLDAP_ACCOUNT_SUFFIX', '@office.ad'),
                'domain_controllers' => explode(' ', env('ADLDAP_CONTROLLERS', '198.19.120.254 192.168.2.254')),
                'port' => env('ADLDAP_PORT', 636),
                'timeout' => env('ADLDAP_TIMEOUT', 5),
                'base_dn' => env('ADLDAP_BASEDN', 'dc=office,dc=ad'),
                'admin_account_prefix' => env('ADLDAP_ADMIN_ACCOUNT_PREFIX', ''),
                'admin_account_suffix' => env('ADLDAP_ADMIN_ACCOUNT_SUFFIX', ''),
                'admin_username' => env('ADLDAP_ADMIN_USERNAME', ''),
                'admin_password' => env('ADLDAP_ADMIN_PASSWORD', ''),
                'follow_referrals' => false,
                'use_ssl' => env('ADLDAP_USE_SSL', true),
                'use_tls' => env('ADLDAP_USE_TLS', false),
            ],
        ],
    ],
];
