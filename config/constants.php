<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Global constants
    |--------------------------------------------------------------------------
    |
    | This option defines the global constants
    |
    */

    'PASSWORD' => '*****',
    'VERSION_IMAGES_PATH' => storage_path('images')
];
