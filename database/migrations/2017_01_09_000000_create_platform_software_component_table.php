<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlatformSoftwareComponentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'platform_software_component', function (Blueprint $table) {
                $table->unsignedInteger('platform_id')->index();
                $table->foreign('platform_id')->references('id')->on('platforms')->onDelete('cascade');
                $table->unsignedInteger('software_component_id')->index();
                $table->foreign('software_component_id')->references('id')->on('software_components')->onDelete('cascade');
                $table->unique(['software_component_id', 'platform_id'], 'psc_software_component_id_platform_id_unique');
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('platform_software_component');
    }
}
