<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSoftwareVersionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'software_versions', function (Blueprint $table) {
                $table->increments('id');
                $table->unsignedInteger('software_branch_id')->index();
                $table->foreign('software_branch_id')->references('id')->on('software_branches')->onDelete('cascade');
                $table->unsignedInteger('software_component_id')->index();
                $table->foreign('software_component_id')->references('id')->on('software_components')->onDelete('cascade');
                $table->string('version', 45);
                $table->string('file_name', 120);
                $table->string('checksum', 65);
                $table->boolean('is_enabled')->default(true);
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('software_versions');
    }
}
