<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePartnersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'partners', function (Blueprint $table) {
                $table->increments('id');
                $table->unsignedInteger('country_id')->index();
                $table->foreign('country_id')->references('id')->on('countries');
                $table->string('name', 45)->unique();
                $table->string('type', 45);
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('partners');
    }
}
