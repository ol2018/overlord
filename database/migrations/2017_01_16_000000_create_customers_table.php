<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'customers', function (Blueprint $table) {
                $table->increments('id');
                $table->unsignedInteger('country_id')->index();
                $table->foreign('country_id')->references('id')->on('countries');
                $table->unsignedInteger('partner_id')->index();
                $table->foreign('partner_id')->references('id')->on('partners');
                $table->string('name', 45)->unique();
                $table->string('description', 255)->nullable();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
