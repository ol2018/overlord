<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeviceHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'device_history', function (Blueprint $table) {
                $table->increments('id');
                $table->unsignedInteger('device_id')->index();
                $table->foreign('device_id')->references('id')->on('devices');
                $table->string('ip', 45);
                $table->string('ptr', 45);
                $table->timestamp('created_at');
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('device_history');
    }
}
