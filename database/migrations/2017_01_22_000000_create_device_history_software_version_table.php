<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeviceHistorySoftwareVersionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'device_history_software_version', function (Blueprint $table) {
                $table->unsignedInteger('device_history_id')->index();
                $table->foreign('device_history_id')->references('id')->on('device_history')->onDelete('cascade');
                $table->unsignedInteger('software_version_id')->index();
                $table->foreign('software_version_id')->references('id')->on('software_versions')->onDelete('cascade');
                $table->unique(['device_history_id', 'software_version_id'], 'dhsv_device_history_id_software_version_id_unique');
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('device_history_software_version');
    }
}
