<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeviceConfigurationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'device_configurations', function (Blueprint $table) {
                $table->increments('id');
                $table->unsignedInteger('device_id')->index();
                $table->foreign('device_id')->references('id')->on('devices');
                $table->string('mgmt_ip', 45)->nullable();
                $table->string('mgmt_login', 45)->nullable();
                $table->string('mgmt_password', 45)->nullable();
                $table->string('ip', 45)->nullable();
                $table->string('system_login', 45)->nullable();
                $table->string('system_password', 45)->nullable();
                $table->string('bios_password', 45)->nullable();
                $table->boolean('has_mgmt')->default(true);
                $table->boolean('has_access')->default(true);
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('device_configurations');
    }
}
