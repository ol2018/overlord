<?php

use App\Models\Platform;
use Illuminate\Database\Seeder;

class PlatformTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $class = Platform::class;
        $data = [
            ['id' => 1, 'name' => 'LM36'],
            ['id' => 2, 'name' => 'LM36B'],
            ['id' => 3, 'name' => 'LM12B'],
            ['id' => 4, 'name' => 'LMDEMO1'],
            ['id' => 5, 'name' => 'LOGM-FW-MS'],
            ['id' => 6, 'name' => 'LOGM-FW-VM'],
            ['id' => 7, 'name' => 'LOGM-FW-HW-I'],
            ['id' => 8, 'name' => 'LOGM-16TB-H'],
            ['id' => 9, 'name' => 'LOGM-48TB-H'],
            ['id' => 10, 'name' => 'LOGM-120TB-H'],
            ['id' => 11, 'name' => 'LOGM-16TB-H-G2'],
            ['id' => 12, 'name' => 'LOGM-48TB-H-G2'],
            ['id' => 13, 'name' => 'LOGM-120TB-H-G2'],
            ['id' => 14, 'name' => 'LOGM-16TB-D'],
            ['id' => 15, 'name' => 'LOGM-48TB-D'],
            ['id' => 16, 'name' => 'LOGM-120TB-D'],
            ['id' => 17, 'name' => 'LOGM-16TB-D-G2'],
            ['id' => 18, 'name' => 'LOGM-48TB-D-G2'],
            ['id' => 19, 'name' => 'LOGM-120TB-D-G2'],
            ['id' => 20, 'name' => 'LMVM1'],
        ];
        
        foreach ($data as $aData) {
            $model = new $class();
            if (!is_null($model1 = $model::find($aData['id']))) {
                $model = $model1;
            }
            $model->fill($aData);
            if (!$model->validate()) {
                $this->command->getOutput()->writeln("<error>Errors: </error>");
                dd($model->getErrors());
            } else {
                $model->save();
            }
        }
    }
}
