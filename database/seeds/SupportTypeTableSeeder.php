<?php

use App\Models\SupportType;
use Illuminate\Database\Seeder;

class SupportTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $class = SupportType::class;
        $data = [
            ['id' => 1, 'name' => 'software'],
            ['id' => 2, 'name' => 'hardware'],
        ];
        foreach ($data as $aData) {
            $model = new $class();
            if (!is_null($model1 = $model::find($aData['id']))) {
                $model = $model1;
            }
            $model->fill($aData);
            if (!$model->validate()) {
                $this->command->getOutput()->writeln("<error>Errors: </error>");
                dd($model->getErrors());
            } else {
                $model->save();
            }
        }
    }
}
