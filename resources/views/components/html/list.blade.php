    <table class="table">
        <thead>
            <tr>
                {{-- iterate table column headers --}}
                <th scope="col"></th>
                @php($filter = false)
                @foreach($cols as $i => $col)
                    <th scope="col">
                        {{-- enable filter column --}}
                        {{ Form::open(['method' => 'get']) }}
                            {{ Form::label($i, array_get($col, '0')) }}<br>
                            @if(array_get($col, '1') === true)
                                {{ Form::text($i, null, ['class' => 'w-100']) }}
                                @php($filter = true)
                            @endif
                        {{ Form::close() }}
                    </th>
                @endforeach
                <th scope="col" style="width:110px;">
                    @if($filter)
                        <a title="{{ _i('Reset filter') }}" href="{{ url()->current() }}" class="btn btn-primary">{{ _i('Reset') }} <i class="fas fa-filter"></i></a>
                    @endif
                </th>
            </tr>
        </thead>
        <tbody>
        @if($data->count() > 0)
        @foreach($data->toArray(0) as $item)
            <tr>
                <th scope="row">{{ $loop->iteration }}</th>
                @foreach($cols as $row => $var)
                    <td>{{ array_get($item, $row) }}</td>
                @endforeach
                @include('components.html.list_buttons')
            </tr>
        @endforeach
        @else
            <tr>
                <td colspan="{{ count($cols) + 2 }}">
                    <div class="alert alert-warning">{{ _i('I don\'t have any records!') }}</div>
                </td>
            </tr>
        @endif
        </tbody>
    </table>
