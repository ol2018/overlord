<td>
    @if(array_has($item, 'edit_button'))
        <div class="float-left">
            <a title="{{ _i('Edit record') }}" class="btn btn-warning" href="{{ array_get($item, 'edit_button') }}"><i class="fas fa-pencil-alt"></i></a>
        </div>
    @endif
    @if(array_has($item, 'device_history_button'))
        <div class="float-right">
            <a title="{{ _i('Device history') }}" class="btn btn-warning" href="{{ array_get($item, 'device_history_button') }}"><i class="fas fa-history"></i></a>
        </div>
    @endif
    @if(array_has($item, 'delete_button'))
        <div class="float-right">
        {{ Form::open(['route' => array_get($item, 'delete_button'), 'method' => 'delete']) }}
            <button title="{{ _i('Delete record') }}" class="btn btn-danger" type="submit" onclick="return confirm('{{ _i('Are you sure?') }}')"><i class="fas fa-times"></i></button>
        {{ Form::close() }}
        </div>
    @endif
</td>
