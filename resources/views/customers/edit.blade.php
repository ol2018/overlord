@extends('layouts.app')
@section('title', _i('Update customer'))
@section('content')
    {{ Form::model(array_get($data, 'resource'), ['route' => ['customers.update', array_get($data, 'id')], 'method' => 'put']) }}
    @include('customers.form_fields')
    {{ Form::close() }}
@endsection
