{{ Form::bsFields(
    [
        'name' => ['text' => [_i('Customer name')]],
        'country_id' => [
            'select' => [
                _i('Country'),
                array_get($data, 'countries'),
                array_get($data, 'country_id'),
            ]
        ],
        'partner_id' => [
            'select' => [
                _i('Partner'),
                array_get($data, 'partners'),
                array_get($data, 'partner_id'),
            ]
        ],
        'description' => ['textarea' => [_i('Description')]],
        'submit' => ['submit' => [_i('Submit')]],
    ]
) }}
