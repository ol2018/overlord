@extends('layouts.app')
@section('title', _i('Update device configuration'))
@section('content')
    {{ Form::model(array_get($data, 'resource'), ['route' => ['device_configurations.update', array_get($data, 'id')], 'method' => 'put']) }}
    @include('device_configurations.form_fields')
    {{ Form::close() }}
@endsection
