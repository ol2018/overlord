{{ Form::bsFields(
    [
        'device_id' => [
            'select' => [
                _i('Device'),
                array_get($data, 'devices'),
                array_get($data, 'device_id'),
            ]
        ],
        'mgmt_ip' => ['text' => [_i('MGMT IP')]],
        'mgmt_login' => ['text' => [_i('MGMT login')]],
        'mgmt_password' => [array_get($data, 'password_input') => [_i('MGMT password')]],
        'ip' => ['text' => [_i('IP')]],
        'system_login' => ['text' => [_i('System login')]],
        'system_password' => [array_get($data, 'password_input') => [_i('System password')]],
        'bios_password' => [array_get($data, 'password_input') => [_i('BIOS password')]],
        'has_mgmt' => ['checkbox' => [_i('Has MGMT')]],
        'has_access' => ['checkbox' => [_i('Has access')]],
        'submit' => ['submit' => [_i('Submit')]],
    ]
) }}
