@extends('layouts.app')
@section('title', _i('Device history list'))
@section('content')
    {{ Html::bsList($data,
    [
        'version' => [_i('Version'), true],
        'last_ip' => [_i('Last IP'), true],
        'last_ptr' => [_i('Last PTR'), true],
        'last_check' => [_i('Last check'), true],
    ], false, false) }}
@endsection
