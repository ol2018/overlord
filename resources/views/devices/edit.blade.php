@extends('layouts.app')
@section('title', _i('Update device'))
@section('content')
    {{ Form::model(array_get($data, 'resource'), ['route' => ['devices.update', array_get($data, 'id')], 'method' => 'put']) }}
    @include('devices.form_fields')
    {{ Form::close() }}
@endsection
