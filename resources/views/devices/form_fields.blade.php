{{ Form::bsFields(
    [
        'customer_id' => [
            'select' => [
                _i('Customer'),
                array_get($data, 'customers'),
                array_get($data, 'customer_id'),
            ]
        ],
        'description' => ['textarea' => [_i('Description')]],
        'submit' => ['submit' => [_i('Submit')]],
    ]
) }}
