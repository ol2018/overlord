@extends('layouts.app')
@section('title', _i('Create partner'))
@section('content')
    {{ Form::open(['route' => 'partners.store']) }}
    @include('partners.form_fields')
    {{ Form::close() }}
@endsection
