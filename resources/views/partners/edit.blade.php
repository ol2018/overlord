@extends('layouts.app')
@section('title', _i('Update partner'))
@section('content')
    {{ Form::model(array_get($data, 'resource'), ['route' => ['partners.update', array_get($data, 'id')], 'method' => 'put']) }}
    @include('partners.form_fields')
    {{ Form::close() }}
@endsection
