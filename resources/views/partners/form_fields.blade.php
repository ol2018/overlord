{{ Form::bsFields(
    [
        'name' => ['text' => [_i('Partner name')]],
        'country_id' => [
            'select' => [
                _i('Country'),
                array_get($data, 'countries'),
                array_get($data, 'country_id'),
            ]
        ],
        'type' => ['text' => [_i('Type')]],
        'submit' => ['submit' => [_i('Submit')]],
    ]
) }}
