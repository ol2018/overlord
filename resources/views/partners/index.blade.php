@extends('layouts.app')
@section('title', _i('Partners list'))
@section('list_buttons')
    <a href="{{ route('partners.create') }}">{{ _i('Create') }}</a>
@endsection
@section('content')
    {{ Html::bsList($data,
    [
        'name' => [_i('Partner name'), true],
        'country' => [_i('Country'), true],
        'type' => [_i('Type'), true],
    ]) }}
@endsection
