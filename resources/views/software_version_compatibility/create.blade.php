@extends('layouts.app')
@section('title', _i('Create software version compatibility for') . ": " . array_get($data, 'software_version_name'))
@section('content')
    {{ Form::open(['route' => ['software_version.compatibility.store', array_get($data, 'software_version_id')]]) }}
        @include('software_version_compatibility.form_fields')
    {{ Form::close() }}
@endsection
