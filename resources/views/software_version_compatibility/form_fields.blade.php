{{ Form::bsFields(
    [
        'min_version_id' => [
            'select' => [
                _i('Minimum version'),
                array_get($data, 'software_versions'),
                array_get($data, 'min_version_id'),
                ['placeholder' => _i('Select none')],
            ]
        ],
        'max_version_id' => [
            'select' => [
                _i('Maximum version'),
                array_get($data, 'software_versions'),
                array_get($data, 'max_version_id'),
                ['placeholder' => _i('Select none')],
            ]
        ],
        'submit' => ['submit' => [_i('Submit')]],
    ]
) }}
