@php($aParams = !is_null(array_get($data, 'id')) ? ['disabled' => 'disabled'] : [])

{{ Form::bsFields(
    [
        'version' => ['text' => [_i('Software version'), $aParams]],
        'file_name' => ['text' => [_i('File name'), $aParams]],
        'software_component_id' => [
            'select' => [
                _i('Software component'),
                array_get($data, 'software_components'),
                array_get($data, 'software_component_id'),
                $aParams
            ]
        ],
        'software_branch_id' => [
            'select' => [
                _i('Software branch'),
                array_get($data, 'software_branches'),
                array_get($data, 'software_branch_id'),
            ]
        ],
        'platforms[]' => [
            'select' => [
                _i('Platforms'),
                array_get($data, 'platforms'),
                array_get($data, 'platform_ids'),
            ]
        ],
        'is_enabled' => ['checkbox' => [_i('Is enabled')]],
        'submit' => ['submit' => [_i('Submit')]],
    ]
) }}
