@extends('layouts.app')
@section('title', _i('Software versions list'))
@section('list_buttons')
    <a href="{{ route('software_versions.create') }}">{{ _i('Create') }}</a>
@endsection
@section('content')
    {{ Html::bsList($data, [
        'version' => [_i('Software version'), true],
        'file_name' => [_i('File name'), true],
        'software_component' => [_i('Software component'), true],
        'software_branch' => [_i('Software branch'), true],
        'platforms' => [_i('Platforms'), true],
        'is_enabled' => [_i('Is enabled'), true],
    ]) }}
@endsection
