{{ Form::bsFields(
    [
        'description' => ['textarea' => [_i('Description')]],
        'expiration_date' => ['date' => [_i('Expiration date')]],
        'support_type_id' => [
            'select' => [
                _i('Support type'),
                array_get($data, 'support_types'),
                array_get($data, 'support_type_id'),
            ]
        ],
        'device_id' => [
            'select' => [
                _i('Device'),
                array_get($data, 'devices'),
                array_get($data, 'device_id'),
            ]
        ],
        'submit' => ['submit' => [_i('Submit')]],
    ]
) }}
