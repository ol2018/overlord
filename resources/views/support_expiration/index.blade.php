@extends('layouts.app')
@section('title', _i('Support expiration list'))
@section('content')
    {{ Html::bsList($data, [
        'description' => [_i('Description'), true],
        'expiration_date' => [_i('Expiration date'), true],
        'support_type' => [_i('Support type'), true],
        'device' => [_i('Device'), true],
    ]) }}
@endsection
