{{ Form::bsFields(
    [
        'name' => ['text' => [_i('User name')]],
        'email' => ['email' => [_i('E-mail address')]],
        'password' => ['password' => [_i('Password')]],
        'ldap' => ['checkbox' => [_i('LDAP')]],
        'submit' => ['submit' => [_i('Submit')]],
    ]
) }}
