<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


$var1 = ['except' => 'show'];
$var2 = ['except' => ['show', 'create']];
$var3 = ['only' => ['index', 'edit', 'update']];
$var4 =  ['only' => ['create', 'store', 'destroy']];

Route::resource('users', 'UserController', $var1);

$this->get('/', 'LoginController@showLoginForm')->name('login');
$this->post('/', 'LoginController@login');
$this->post('logout', 'LoginController@logout')->name('logout');

Route::get('current_user', 'UserController@getUser')->name('current_user');
Route::resource('customers', 'CustomerController', $var1);
Route::resource('partners', 'PartnerController', $var1);
Route::resource('support_expiration', 'SupportExpirationController', $var2);
Route::resource('software_versions', 'SoftwareVersionController', $var1);
Route::resource('software_version.compatibility', 'SoftwareVersionCompatibilityController', $var4);
Route::get('device_history/{device}/history', 'DeviceHistoryController@history')->name('device_history.history');
Route::resource('device_configurations', 'DeviceConfigurationController', $var1);

Route::resource('devices', 'DeviceController', $var3);
Route::post('1/device/register', 'DeviceController@register')->name('register');
Route::post('1/update', 'DeviceController@updateDevice')->name('update_device');

Route::get('images/{name}', 'SoftwareVersionController@downloadImage')->name('images');
